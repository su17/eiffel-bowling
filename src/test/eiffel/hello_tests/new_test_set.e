note
	description: "[
		Eiffel tests that can be executed by testing tool.
	]"
	author: "EiffelStudio test wizard"
	date: "$Date$"
	revision: "$Revision$"
	testing: "type/manual"

class
	NEW_TEST_SET

inherit

	EQA_TEST_SET

feature -- Test routines

	new_test_routine
		local
			h: HELLO
		do
			create h.make
			assert ("The answer is not right", h.answer = 42)
		end

end
